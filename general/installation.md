# Installation

If you haven’t already, start by downloading the program from
[here](http://codehaus.wohlsoft.ru/downloads.html).

The installer is directly created from a ZIP archive of
our repository and will deploy the project into the directory you
specify. The package will be installed to “$TARGETDIR/SMBX2”, but you’re
free to change the name of the folder afterwards.

## Add\_leveltest\_assoc.bat

The script of this name in the data folder can be used in order to
enable you to open lvl and lvlx files by double-clicking them, without
the need to go through the editor.