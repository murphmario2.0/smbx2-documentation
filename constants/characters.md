# Character constants

Constants for the character names.

| Constant | Value | Description |
| --- | --- | --- |
| CHARACTER_MARIO | 1 | Mario. |
| CHARACTER_LUIGI | 2 | Luigi. |
| CHARACTER_PEACH | 3 | Peach. |
| CHARACTER_TOAD | 4 | Toad. |
| CHARACTER_LINK | 5 | Link. |
| CHARACTER_MEGAMAN | 6 | Megaman. |
| CHARACTER_WARIO | 7 | Wario. |
| CHARACTER_BOWSER | 8 | Bowser. |
| CHARACTER_KLONOA | 9 | Klonoa. |
| CHARACTER_NINJABOMBERMAN | 10 | Ninja Bomberman. |
| CHARACTER_ROSALINA | 11 | Rosalina. |
| CHARACTER_SNAKE | 12 | Snake. |
| CHARACTER_ZELDA | 13 | Zelda. |
| CHARACTER_ULTIMATERINKA | 14 | Ultimate Rinka. |
| CHARACTER_UNCLEBROADSWORD | 15 | Uncle Broadsword. |
| CHARACTER_SAMUS | 16 | Samus. |